/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigos;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import pruebatecnica.principal;
/**
 *
 * @author alexander
 */
public class conexion_consulta {
    static Connection conexion=null;
    static Statement sentencia;
    static ResultSet resultado;
    static ResultSetMetaData resultadometa;
//------------------------------------------------------------------------------
// CONECTAR A LA BASE DE DATOS
//------------------------------------------------------------------------------
public static void conectar(){
        String ruta="jdbc:mysql://localhost/db_prueba";
        String user="root";
        String pass="";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion=DriverManager.getConnection(ruta,user,pass); 
            sentencia= conexion.createStatement();
            System.out.println("Conectado");
        } catch (Exception e) {
            System.out.println("No conectado");
        }
    }
//------------------------------------------------------------------------------
// insertar tabla
//------------------------------------------------------------------------------
public static void insertar_registro(String cad1,String cad2,String cad3,String cad4){
     try {
            sentencia = conexion.createStatement();
            sentencia.executeUpdate("INSERT INTO usuario VALUES ('"+cad1+"','"+cad2+"','"+cad3+"','"+cad4+"')");
            JOptionPane.showMessageDialog(null, "El usuario, ha sido registrado con exito!");  
        } catch (SQLException ex) {
            Logger.getLogger(principal.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se puede resgistrar el usuario...");
        }
    }
//------------------------------------------------------------------------------
// actualizar registro 
//------------------------------------------------------------------------------
public static void actualizar_registro(String cad1,String cad2,String cad3,String cad4){
     try {
            Statement sentencia3 = conexion.createStatement();
            sentencia3.executeUpdate("UPDATE usuario SET id_rol='"+cad2+"', nombre='"+cad3+"', activo='"+cad4+"' WHERE id_usuario ='"+cad1+"'");
            System.out.println(sentencia3);
            JOptionPane.showMessageDialog(null, "El usuario, ha sido actualizado con exito!");  
        } catch (SQLException ex) {
            Logger.getLogger(principal.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se puede actualizar el usuario...");
        }
    }
//------------------------------------------------------------------------------
// Borrar registro
//------------------------------------------------------------------------------
public static void borrar_registro(String cad1){
    //sentencia.close();
    try {
            sentencia = conexion.createStatement();
            sentencia.executeUpdate("DELETE FROM usuario WHERE ID_USUARIO = '"+cad1+"'");
            JOptionPane.showMessageDialog(null, "El usuario, ha sido eliminado con exito!");  
        } catch (SQLException ex) {
            Logger.getLogger(principal.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se puede eliminar el usuario...");
        }
    }
//------------------------------------------------------------------------------
// BUSCAR registro
//------------------------------------------------------------------------------
public static boolean buscar_registro(String cad1){
      boolean resul = false;
      try {
            String q = "SELECT * FROM usuario WHERE nombre = '"+cad1+"'";
            resultado=sentencia.executeQuery(q);
            resultadometa= resultado.getMetaData();
            if (resultado.next()){
               resul = true; 
            }
            //JOptionPane.showMessageDialog(null, "El usuario, ha sido encontrado!");  
        } catch (SQLException ex) {
            Logger.getLogger(principal.class.getName()).log(Level.SEVERE, null, ex);
            //JOptionPane.showMessageDialog(null, "No se puede encontrar el usuario...");
            resul =false;
        }
     return(resul);
    }
//------------------------------------------------------------------------------
// LLENAR COMBOBOX
//------------------------------------------------------------------------------
public static ArrayList<String>llenar_combo(String xcampo){
         ArrayList<String> lista = new ArrayList<String>();
         String q = "SELECT * FROM rol";
        try {
            resultado=sentencia.executeQuery(q);
        } catch (Exception e) {
           } 
        try {
             while(resultado.next()){
             lista.add(resultado.getString(xcampo));
             }
            }
        catch (Exception e) {
        } 
        return lista;
    }
//------------------------------------------------------------------------------
// LLENAR TABLA
//------------------------------------------------------------------------------
public static ArrayList<Object[]> llenar_tabla(){
        ArrayList<Object[]> datos = new ArrayList<Object[]>();
        String q = "SELECT * FROM usuario";
        try {
            resultado=sentencia.executeQuery(q);
            resultadometa= resultado.getMetaData();
            System.out.println("Correcto");
        } catch (Exception e) {
            System.out.println("No Correcto");
        }
        try {
            while(resultado.next()){
                Object[] filas = new Object[resultadometa.getColumnCount()];
                for(int i = 0;i<resultadometa.getColumnCount();i++){
                    filas[i]= resultado.getObject(i+1);
                }
                datos.add(filas);
            }
        } catch (Exception e) {
        }
        return datos;
    }
//------------------------------------------------------------------------------
// BUSCAR EN BASE DE DATOS Y LLENAR TABLA
//------------------------------------------------------------------------------
public static ArrayList<Object[]> buscar_tabla(String xcampo, String variable){
        ArrayList<Object[]> datos = new ArrayList<Object[]>();
        //String q = "SELECT * FROM t_paciente WHERE id = "+xcampo+"";
        variable = "'%"+variable+"%'";
        String q = "SELECT * FROM usuario WHERE "+xcampo+" LIKE "+variable+"";
        try {
            resultado=sentencia.executeQuery(q);
            resultadometa= resultado.getMetaData();
            //System.out.println(q);
            //System.out.println("Correcto");
        } catch (Exception e) {
            //System.out.println("No Correcto");
            //System.out.println(q);
        }
        try {
            while(resultado.next()){
                Object[] filas = new Object[resultadometa.getColumnCount()];
                for(int i = 0;i<resultadometa.getColumnCount();i++){
                    filas[i]= resultado.getObject(i+1);
                }
                datos.add(filas);
            }
        } catch (Exception e) {
        }
        return datos;
    
    }
}